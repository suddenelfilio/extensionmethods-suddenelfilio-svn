﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Suddenelfilio.ExtensionMethods.DateTime
{
    public static class DateTimeExtensions
    {
        public static int WeekNumber(this System.DateTime value)
        {
            return CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(value, CalendarWeekRule.FirstFourDayWeek,
                                                                     DayOfWeek.Monday);

        }
    }
}
