﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Suddenelfilio.ExtensionMethods.IO
{
    /// <summary>
    /// Contains extension methods for the <see cref="System.IO.FileInfo"/> class.
    /// </summary>
    public static class FileInfoExtensions
    {
        public static string GetMimeType(this FileInfo value)
        {
            return StringExtensions.GetMimeType(value.FullName);
        }
    }
}
