﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Suddenelfilio.ExtensionMethods.IO
{
    public static class StringExtensions
    {

        public static string GetMimeType(this string fileName)
        {

            string fileExtension = System.IO.Path.GetExtension(fileName).ToLower();
            
            var valueInTable = MimeTypeTable.MimeTypes.Value[fileExtension];
            if (valueInTable != null)
                return valueInTable as string;

            Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(fileExtension);
            if (rk != null && rk.GetValue("Content Type") != null)
                return rk.GetValue("Content Type").ToString();

            return null;
        }
    }
}
