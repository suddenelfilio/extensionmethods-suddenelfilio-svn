﻿using System;

namespace Suddenelfilio.ExtensionMethods.String
{
    public static class DecimalExtensions
    {
        public static  string ToEuro(this decimal value)
        {
            return string.Concat(Convert.ToChar(8364), " ", value.ToString());
        }
    }
}
