﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Suddenelfilio.ExtensionMethods.String
{
	public static class StringExtensions
	{
        public static string Capitalize(this string value)
        {
            return string.Concat(value.Substring(0, 1).ToUpper(), value.Substring(1));
        }
	}
}
