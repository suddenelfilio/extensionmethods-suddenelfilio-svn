﻿using System;
using System.IO;
using Suddenelfilio.ExtensionMethods.DateTime;
using Suddenelfilio.ExtensionMethods.IO;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //Using a string with a filename as value.
            string name = "d:\\test.txt";
            Console.WriteLine("mimetype for file {0} is {1}", name, name.GetMimeType());

            FileInfo info = new FileInfo("d:\\test.txt");
            Console.WriteLine("mimetype for file {0} is {1}", info.FullName, info.GetMimeType());

            Console.WriteLine("{0} is in week {1}", DateTime.Now, DateTime.Now.WeekNumber());

            Console.ReadLine();
        
        }
    }
}
